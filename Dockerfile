FROM nginx:1.16.0-alpine
ADD /venus /var/www
COPY nginx.conf /etc/nginx/nginx.conf
CMD ["nginx", "-g", "daemon off;"]